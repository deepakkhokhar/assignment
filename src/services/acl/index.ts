import * as express from 'express';
import cache from '../../utils/cache';
import { cache as cacheConstant } from '../../config/constants';
import AclDBService from '../../database/service/acl.dbservice';
const { prefix, rolePolicyModule, apiPolicyModule } = cacheConstant;
export default class ACLService {

    /**
   *
   *
   * @static
   * @param {string} roleName
   * @returns
   * @memberof ACLService
   */
    static async getRolePolicyMapping(roleName: string) {
        const list = await cache.getList(`${prefix}${rolePolicyModule}`, roleName);
        if (list && list.length) {
            return list;
        }
        const rolePolicyMapperList = await AclDBService.getRolePolicyMapping(roleName);
        const policyList = rolePolicyMapperList.map((obj) => obj.policyName);
        await cache.insertList(`${prefix}${rolePolicyModule}`, roleName, policyList);
        return policyList;
    }

    /**
    *
    *
    * @static
    * @param {string} apiName
    * @returns
    * @memberof ACLService
    */
    static async getApiPolicyMapping(apiName: string) {
        const list = await cache.getList(`${prefix}${apiPolicyModule}`, apiName);
        console.log("`${prefix}${apiPolicyModule}`", `${prefix}${apiPolicyModule}`)
        if (list && list.length) {
            return list;
        }
        const apiPolicyMapperList = await AclDBService.getApiPolicyMapping(apiName);
        const policyList = apiPolicyMapperList.map((obj) => obj.policyName);
        await cache.insertList(`${prefix}${apiPolicyModule}`, apiName, policyList);
        return policyList;
    }
}
