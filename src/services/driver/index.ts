import DriverDBService from '../../database/service/driver.dbservice';

const serviceName = '[DriverService]';

export default class DriverService {



  static async getCityList() {
    const result = await DriverDBService.getCityList();
    return result;
  }

  static async getLanguageList() {
    const result = await DriverDBService.getLanguageList();

    return result;
  }

}