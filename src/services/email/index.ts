import EmailTemplateDBService from '../../database/service/emailTemplate.dbService';
import { cache } from '../../config/constants';
import { vars } from '../../config/vars';
import RedisCache from '../../utils/cache';
import EmailSender from '../../utils/email';

const { emailPrefix } = cache;
const { frontendHost } = vars;

export default class EmailService {
    static async SendEmail(user: any, type: string, variables = {}) {
        let mailDetails = await RedisCache.get(emailPrefix, type);
        if (!mailDetails) {
            mailDetails = await EmailService.loadEmailTemplateToRedis(type);
        }
        else {
            mailDetails = JSON.parse(mailDetails);
        }
        if (!mailDetails || !mailDetails.status) {
            return;
        }

        variables['[LOCALHOST]'] = frontendHost;

        Object.keys(variables).forEach((k) => {
            mailDetails.body = mailDetails.body.split(k).join(variables[k]);
        });
        const emailObj = {
            to: user.email,
            subject: mailDetails.subject,
            html: mailDetails.body
        };
        const result = await EmailSender.send(emailObj);
        return result;
    }

    static async loadEmailTemplateToRedis(emailType = null) {
        const emailList = await EmailTemplateDBService.getAllEmailTemplates();
        let emailTemplate = null;
        emailList.forEach((email: any) => {
            const { type, ...rest } = email;
            if (emailType && type === emailType) {
                emailTemplate = rest;
            }
            RedisCache.set(emailPrefix, type, JSON.stringify(rest));
        });
        return emailList;
    }
}
