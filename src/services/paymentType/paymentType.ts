import PaymentTypeDBService from '../../database/service/paymentType.dbservice';

const serviceName = '[PaymentTypeService]';

export default class DriverService {



    static async getPaymentType() {
        const result = await PaymentTypeDBService.getPaymentType();
        return result;
    }

}