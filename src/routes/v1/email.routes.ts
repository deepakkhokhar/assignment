import * as express from 'express';
import EmailController from '../../controllers/email';
import jwt from '../../middlewares/auth';

const router = express.Router();
router.route('/loadEmailTemplates').get(jwt.verify, EmailController.loadEmailTemplate);
router.route('/sendEmail').post(jwt.verify, EmailController.sendEmail);

export default router;
