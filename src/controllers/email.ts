import * as express from 'express';
import CustomResponse from '../utils/response';
import EmailServices from '../services/email/index'
/**
 *
 *
 * @export
 * @class EmailController
 */
export default class EmailController {

  static async loadEmailTemplate(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction

  ): Promise<void> {
    try {
      const result = await EmailServices.loadEmailTemplateToRedis();
      const response = new CustomResponse(res);
      return response.setResponse({ result });
    }
    catch (error) {
      return next(error);
    }
  }

  static async sendEmail(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction

  ): Promise<void> {
    try {
      const reqBody = { ...req.body };
      const userInfo = {
        email: reqBody.email
      }
      const result = await EmailServices.SendEmail(userInfo, reqBody.type, reqBody.variable);
      const response = new CustomResponse(res);
      return response.setResponse({ result });
    }
    catch (error) {
      return next(error);
    }
  }
}
